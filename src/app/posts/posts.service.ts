import { Post } from './post.model';
import { Injectable } from '@angular/core';
// subject from rxjs
// http://reactivex.io/rxjs/manual/overview.html
import { Subject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';

@Injectable({
  // we declare that this service should be created
  // by the root application injector.
  providedIn: 'root',
})

export class PostService {
  // new Post obj from post.model
  // declare private var posts, it's an empty array of Post
  private posts: Post[] = [];
  // delare postUpdate as new subect: an empty array of Post
  private postUpdated = new Subject<{posts: Post[], postCount: number}>();
  // construct the class Postservice, add two private properties http and router
  constructor( private http: HttpClient, private router: Router ) {

  }
  // getPosts method/ function
  getPosts(postPerPage: number, currentPage: number) {
    // get message and posts from url using the http
    // The HttpClient.get() method normally returns an observable that either emits the data or an error.
    // Some folks describe it as a "one and done" observable.
    const queryParams = `?pageSize=${postPerPage}&page=${currentPage}`;
    this.http
      .get<{message: string, posts: any, maxPosts: number}>('http://localhost:3300/api/posts' + queryParams)
      .pipe(
        map(postData => {
          return {posts: postData.posts.map(post => {
            return {
              title: post.title,
              content: post.content,
              id: post._id,
              imagePath: post.imagePath,
              creator: post.creator
            };
        }), maxPosts: postData.maxPosts};
      })
    )
    .subscribe((transformedPostData) => {
      this.posts = transformedPostData.posts;
      // copy of orgial array this.posts
      // gen.next(value)
      this.postUpdated.next({posts: [...this.posts], postCount: transformedPostData.maxPosts});
    });
  }

  getPostUpdatedListener() {
    return this.postUpdated.asObservable();
  }
  getPost( id: string) {
    // return {...this.posts.find( p => p.id === id)};
    return this.http.get<{
      _id: string,
      title: string,
      content: string,
      imagePath: string,
      creator: string
    }>('http://localhost:3300/api/posts/' + id);
  }
  addPost( title: string, content: string, image: File) {
    const postData = new FormData();
    postData.append('title', title);
    postData.append('content', content);
    postData.append('image', image, title);

    this.http
      .post<{ message: string, post: Post}>('http://localhost:3300/api/posts',
       postData
      )
      .subscribe((res) => {
        // const post: Post = {
        //   id: res.post.id,
        //   title: title,
        //   content: content,
        //   imagePath: res.post.imagePath
        // };
        // this.posts.push(post);
        // this.postUpdated.next([...this.posts]);
        this.router.navigate(['/']);
    });
  }
  updatePost(id: string, title: string, content: string, image: File | string) {
    // if the image is the object, we send a formdata
    let postData: Post | FormData;
    if ( typeof(image) === 'object' ) {
      postData = new FormData();
      postData.append('id', id);
      postData.append( 'title', title);
      postData.append( 'content', content);
      postData.append( 'image', image, title );

    } else {
      // if the image is the string
      postData = {
        id: id,
        title: title,
        content: content,
        imagePath: image,
        creator: null
      };

    }

    this.http.put('http://localhost:3300/api/posts/' + id, postData)
      .subscribe(res => {
        // const updatedPosts = [...this.posts];
        // const oldPostIndex = updatedPosts.findIndex( p => p.id === id );
        // const post: Post =  {
        //   id: id,
        //   title: title,
        //   content: content,
        //   imagePath: ''
        // };
        // updatedPosts[oldPostIndex] = post;
        // this.postUpdated.next([...this.posts]);
        this.router.navigate(['/']);
      });
  }
  deletePost(postId: string) {
    return this.http
      .delete('http://localhost:3300/api/posts/' + postId);
      // .subscribe(() => {
      //   const updatedPosts = this.posts.filter( post => post.id !== postId);
      //   this.posts = updatedPosts;
      //   this.postUpdated.next([...this.posts]);
      //   // or just call getPosts function again for update the posts
      //   // this.getPosts();
      // });
  }
}
