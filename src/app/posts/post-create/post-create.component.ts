import { Component, OnInit, OnDestroy} from '@angular/core';
import { FormGroup, FormControl , Validators } from '@angular/forms';
import { PostService } from '../posts.service';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { Post } from '../post.model';
import { mimeType } from './mime-type.validator';
import { Subscription } from 'rxjs';
import { AuthService } from 'src/app/auth/auth.service';

@Component({
  selector: 'app-post-create-component',
  templateUrl: './post-create.component.html',
  styleUrls: ['./post-create.component.css']
})


export class PostCreateComponent implements OnInit, OnDestroy {

  post: Post;
  // create new reactive form
  postCreateForm: FormGroup;
  imagePreview: string;
  isLoading = false;
  private mode = 'create';
  private postId: string;
  private authStatusSub: Subscription;

  constructor(
    public postsService: PostService,
    public route: ActivatedRoute,
    private authService: AuthService
  ) {}

  ngOnInit() {
    this.authStatusSub = this.authService.getAuthStatusListenser().subscribe( authStatus => {
      this.isLoading = false;
    });
    // init the reactive form in angualr using FormGroup
    // add FormCOntorl for each field
    this.postCreateForm = new FormGroup({
      title: new FormControl(
        // default value is null
        null,
        // add validators in an array of objs
        {validators: [
          Validators.required,
          Validators.minLength(3)
        ]}
      ),
      content: new FormControl(
        null,
        {validators: [
         Validators.required
        ]}
      ),
      image: new FormControl(
        null, {
          validators: [Validators.required],
          asyncValidators: [mimeType]
        }
      )
    });
    this.route.paramMap.subscribe( (paramMap: ParamMap) => {
      // Map.prototype.has()
      // .has method return a boolean
      if (paramMap.has('postId')) {
        this.mode = 'edit';
        this.postId = paramMap.get('postId');
        this.isLoading = true;
         this.postsService.getPost(this.postId).subscribe( postData => {
          this.isLoading = false;
          this.post = {
            id: postData._id,
            title: postData.title,
            content: postData.content,
            imagePath: postData.imagePath,
            creator: postData.creator
          };
          this.postCreateForm.setValue({
            title: this.post.title,
            content: this.post.content,
            image: this.post.imagePath
          });
         });
      } else {
        this.mode = 'create';
        this.postId = null;
      }
    });
  }
  onImagePicked(event: Event ) {
    // conver the event target to htmlinputelement for ts
    const file = (event.target as HTMLInputElement).files[0];
    // pathValue for single contorl
    this.postCreateForm.patchValue({image: file});
    this.postCreateForm.get('image').updateValueAndValidity();
    const reader = new FileReader();
    reader.onload = () => {
      if (typeof reader.result === 'string' ) {
        this.imagePreview = reader.result;
      }
    };
    reader.readAsDataURL(file);
  }
  onSavePost() {
    if (this.postCreateForm.invalid) {
      return;
    }
    this.isLoading = true;
    if ( this.mode === 'create' ) {
      this.postsService.addPost(
        this.postCreateForm.value.title,
        this.postCreateForm.value.content,
        this.postCreateForm.value.image);
    } else {
      this.postsService.updatePost(
        this.postId,
        this.postCreateForm.value.title,
        this.postCreateForm.value.content,
        this.postCreateForm.value.image
        );
    }
    // reset form
    this.postCreateForm.reset();
  }
  ngOnDestroy() {
    this.authStatusSub.unsubscribe();
  }
}
