import { Component, OnInit, OnDestroy } from '@angular/core';
import { AuthService } from '../auth/auth.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeadComponent implements OnInit, OnDestroy {
  userIsAutheneticated = false;
  private authListenerSubs: Subscription;
  constructor(private authService: AuthService) {}

  ngOnInit() {
    this.userIsAutheneticated = this.authService.getIsAuth();
    this.authListenerSubs = this.authService.getAuthStatusListenser().subscribe( isAuthenticated => {
      this.userIsAutheneticated = isAuthenticated;
    });
  }
  onLogout() {
    this.authService.logout();
  }
  ngOnDestroy() {
    this.authListenerSubs.unsubscribe();
  }
}
