import { Injectable } from '@angular/core';
import { AuthData } from './auth-data.model';
import { HttpClient } from '@angular/common/http';
import { Subject } from 'rxjs';
import { Router } from '@angular/router';

@Injectable({ providedIn: 'root' })

export class AuthService {

  private token: string;
  private isAuthenticated = false;
  private tokenTimber: any;
  private userId: string;
  private authStatusListener = new Subject<boolean>();

  constructor( private http: HttpClient, private router: Router) { }

  getToken() {
    return this.token;
  }

  getIsAuth() {
    return this.isAuthenticated;
  }

  getUserId() {
    return this.userId;
  }

  getAuthStatusListenser() {
    return this.authStatusListener.asObservable();
    }

  createUser(email: string, password: string) {

    const authData: AuthData = {
      email: email,
      password: password
    };

    this.http.post('http://localhost:3300/api/users/signup', authData)
    .subscribe(() => {
      this.router.navigate(['/']);
    }, error => {
      this.authStatusListener.next(false);
    });

  }


  login(email: string, password: string) {

    const authData: AuthData = {
      email: email,
      password: password
    };

    this.http.post<{token: string, expiresIn: number, userId: string}>('http://localhost:3300/api/users/login', authData)
    .subscribe( response => {
      const token = response.token;
      this.token = token;

      if (token) {
        const expiresInDuration = response.expiresIn;

        this.setAuthTimer(expiresInDuration);
        this.isAuthenticated = true;
        this.userId = response.userId;
        this.authStatusListener.next(true);
        const now = new Date();
        const expirationDate = new Date(now.getTime() + (expiresInDuration * 1000));
        this.saveAuthData(token, expirationDate, this.userId);
        this.router.navigate(['/']);
      }
    }, error => {
      this.authStatusListener.next(false);
    });

  }

  autoAuthUser() {
    const authinformation = this.getAuthData();

    if (!authinformation) {
      return;
    }
    const now = new Date();
    const expiresIn = authinformation.expirationDate.getTime() - now.getTime();

    if (expiresIn > 0) {
      this.token = authinformation.token;
      this.isAuthenticated = true;
      this.userId = authinformation.userId;
      this.setAuthTimer(expiresIn / 1000);
      this.authStatusListener.next(true);
    }
  }

  logout() {
    this.token = null;
    this.isAuthenticated = false;
    this.authStatusListener.next(false);
    this.userId = null;
    clearTimeout(this.tokenTimber);
    this.router.navigate(['/']);
    this.clearAuthData();
  }

  private setAuthTimer(duration: number) {
    this.tokenTimber = setTimeout(() => {
      this.logout();
    }, duration * 1000);
  }
  private saveAuthData( token: string, expirationDate: Date, userId: string) {
    localStorage.setItem('token', token);
    localStorage.setItem('expiration', expirationDate.toISOString());
    localStorage.setItem('userId', userId);
  }

  private clearAuthData() {
    localStorage.removeItem('token');
    localStorage.removeItem('expiration');
    localStorage.removeItem('userId');
  }

  private getAuthData() {
    const token = localStorage.getItem('token');
    const expirationDate = localStorage.getItem('expiration');
    const userId = localStorage.getItem('userId');
    if (!token || !expirationDate || !userId) {
      return;
    }
    return {
      token: token,
      expirationDate: new Date(expirationDate),
      userId: userId
    };
  }
}
